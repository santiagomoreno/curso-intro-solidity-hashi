const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("HashiToken", function () {
    let owner, user, user_2, user_3, user_4;

    beforeEach(async function () {
        [owner, user, user_2, user_3, user_4] = await ethers.getSigners();

        const HashiToken = await ethers.getContractFactory("HashiToken", owner);
        this.hashi = await HashiToken.deploy();
    });

    describe("Hahi functions", function () {
        it("Should revert when dont send gas", async function () {
            await expect(this.hashi.connect(user).mint()).to.be.revertedWith("There is no gas");
        });

        it("Should mint 1 hashis if you send 1 ether", async function () {
            await this.hashi.connect(user).mint({value: ethers.utils.parseEther("1")});
            const balance = await this.hashi.balanceOf(user.address);

            expect(ethers.utils.parseEther("1")).to.eq(balance);
        });

        it("Should mint 0.375 hashis if you send 0.375 ether", async function () {
            await this.hashi.connect(user).mint({value: ethers.utils.parseEther("0.375")});
            const balance = await this.hashi.balanceOf(user.address);

            expect(ethers.utils.parseEther("0.375")).to.eq(balance);
        });

        it("Should revert when try to burn more tokens than I have", async function () {
            await this.hashi.connect(user).mint({value: ethers.utils.parseEther("5")});

            await expect(this.hashi.connect(user).burn(ethers.utils.parseEther("50"))).to.be.revertedWith("ERC20: burn amount exceeds balance");
        });

        it("Should revert when try to burn a little bit more tokens than I have", async function () {
            await this.hashi.connect(user).mint({value: ethers.utils.parseEther("5")});

            await expect(this.hashi.connect(user).burn(ethers.utils.parseEther("5.0001"))).to.be.revertedWith("ERC20: burn amount exceeds balance");
        });

        it("Should recieve ether if burn tokens", async function () {
            const amount = ethers.utils.parseEther("1000");

            await this.hashi.connect(user).mint({value: amount});
            const etherBeforeBalance = await ethers.provider.getBalance(user.address);

            await this.hashi.connect(user).burn(amount);
            const etherAfterBalance = await ethers.provider.getBalance(user.address);
        
            expect(etherAfterBalance).to.be.above(etherBeforeBalance);
        });

        it("Should loss tokens if burn tokens", async function () {
            const amount = ethers.utils.parseEther("1000");

            await this.hashi.connect(user).mint({value: amount});
            const etherBeforeBalance = await this.hashi.balanceOf(user.address);

            await this.hashi.connect(user).burn(amount);
            const etherAfterBalance = await this.hashi.balanceOf(user.address);
            
            expect(etherBeforeBalance).to.be.above(etherAfterBalance);
        });
    });
});