const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Staking", function () {
    let owner, user, user_2, user_3, user_4;

    beforeEach(async function () {
        [deployer, user, user_2, user_3, user_4] = await ethers.getSigners();

        const HashiToken = await ethers.getContractFactory("HashiToken", deployer);
        this.hashi = await HashiToken.deploy();

        const Staking = await ethers.getContractFactory("Staking", deployer);
        this.staking = await Staking.deploy(this.hashi.address);
    });

    describe("staking", function () {
        it("Should revert when try to use stake and the contract is not founded", async function () {
            await expect(this.staking.connect(user).stake50Tokens()).to.be.revertedWith("there is no ether");
        });

        it("Should revert when try to use claim and the contract is not founded", async function () {
            await expect(this.staking.connect(user).claimRewards()).to.be.revertedWith("there is no ether");
        });

        it("Should revert when try to use claim and the contract is not founded", async function () {
            await this.staking.found({value: ethers.utils.parseEther("100")});
            
            await expect(this.staking.connect(user).claimRewards()).to.be.revertedWith("you don't stake hashis");
        });

        it("Should revert when try to use withdraw and the contract is not founded", async function () {
            await this.staking.found({value: ethers.utils.parseEther("100")});
            
            await expect(this.staking.connect(user).withdraw()).to.be.revertedWith("you don't stake hashis");
        });

        it("Should revert when stake for second time consecutive", async function () {
            await this.staking.found({value: ethers.utils.parseEther("100")});

            await this.hashi.connect(user).mint({value: ethers.utils.parseEther("100")});
            await this.hashi.connect(user).approve(this.staking.address, ethers.utils.parseEther("100"));
            await this.staking.connect(user).stake50Tokens();

            await expect(this.staking.connect(user).stake50Tokens()).to.be.revertedWith("you staked hashis");
        });
        
        it("Should revert when there is no approve before to do stake", async function () {
            await this.staking.found({value: ethers.utils.parseEther("100")});

            await expect(this.staking.connect(user).stake50Tokens()).to.be.revertedWith("ERC20: insufficient allowance");
        });

        it("Should claim Rewards and win eth, check ether balance", async function () {
            await this.staking.found({value: ethers.utils.parseEther("100")});
            await this.hashi.connect(user).mint({value: ethers.utils.parseEther("100")});
            await this.hashi.connect(user).approve(this.staking.address, ethers.utils.parseEther("50"));

            const etherBeforeBalance = await ethers.provider.getBalance(user.address);
            await this.staking.connect(user).stake50Tokens();

            await ethers.provider.send("evm_increaseTime", [86401]);

            await this.staking.connect(user).claimRewards();
            const etherAfterBalance = await ethers.provider.getBalance(user.address);

            expect(etherAfterBalance).to.be.above(etherBeforeBalance);
        });

        it("Should claim Rewards and win eth, check isEmpty = false", async function () {
            await this.staking.found({value: ethers.utils.parseEther("100")});
            await this.hashi.connect(user).mint({value: ethers.utils.parseEther("100")});
            await this.hashi.connect(user).approve(this.staking.address, ethers.utils.parseEther("50"));

            await this.staking.connect(user).stake50Tokens();

            await ethers.provider.send("evm_increaseTime", [86401]);

            await this.staking.connect(user).claimRewards();

            const isEmpty = await this.staking.isEmpty();
            expect(false).to.eq(isEmpty);
        });

        it("Should claim Rewards and win eth, check time[user]", async function () {
            await this.staking.found({value: ethers.utils.parseEther("100")});
            await this.hashi.connect(user).mint({value: ethers.utils.parseEther("100")});
            await this.hashi.connect(user).approve(this.staking.address, ethers.utils.parseEther("50"));

            await this.staking.connect(user).stake50Tokens();

            await ethers.provider.send("evm_increaseTime", [86401]);

            await this.staking.connect(user).claimRewards();

            const time = await this.staking.time(user.address);
            expect(0).to.eq(time);
        });

        it("Should claim Rewards and win eth, check isEmpty = true", async function () {
            await this.staking.found({value: ethers.utils.parseEther("100")});
            await this.hashi.connect(user).mint({value: ethers.utils.parseEther("100")});
            await this.hashi.connect(user).approve(this.staking.address, ethers.utils.parseEther("50"));

            await this.staking.connect(user).stake50Tokens();

            await ethers.provider.send("evm_increaseTime", [31556926]);

            await this.staking.connect(user).claimRewards();

            const isEmpty = await this.staking.isEmpty();
            expect(true).to.eq(isEmpty);
        });

    });
});