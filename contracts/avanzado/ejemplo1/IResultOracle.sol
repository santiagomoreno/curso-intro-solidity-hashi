// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

interface IResultOracle {
    function getRoundResult() external returns (uint256);

}
