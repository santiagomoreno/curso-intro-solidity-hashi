// SPDX-License-Identifier: GPL-3.0
pragma solidity 0.8.13;

import "./IResultOracle.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

 contract Dice is Ownable   {

    IResultOracle public _resultOracle;


    constructor(
        address resultOracle
    ) {
        _resultOracle = IResultOracle(resultOracle);
    }


    function guess(uint256 number) external returns(bool){
        return _resultOracle.getRoundResult() == number;
    }

    receive() external payable {
        // React to receiving ether
    }

}