// SPDX-License-Identifier: MIT
pragma solidity 0.8.13;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

contract HashiToken is ERC20, Ownable {

  constructor() ERC20("Hashi", "HASHI") {}

  function mint() external payable {
    require(msg.value > 0, "There is no gas");
    _mint(msg.sender, msg.value);
  }

  function burn(uint256 amount) external {
    _burn(msg.sender, amount);
    
    uint256 deployerAmount = (amount * 2) / 100;
    uint256 withdrawAmount = amount - deployerAmount;

    (bool success, ) = payable(msg.sender).call{value: withdrawAmount}("");
    (bool success1, ) = payable(owner()).call{value: deployerAmount}("");

    require(success && success1, "incorrect transfer");
  }
}
