// SPDX-License-Identifier: MIT
pragma solidity 0.8.13;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

contract Staking is Ownable {

    address public hashiContract;
    bool public founded;
    bool public isEmpty;
    mapping (address => bool) public balance;
    mapping (address => uint256) public time;

    constructor(address _hashi) {
        hashiContract = _hashi;
    }

    function found() external payable onlyOwner {
        require(msg.value == 100 ether, "incorrect amount of gas");
        founded = true;
    }

    function stake50Tokens() external {
        require(founded, "there is no ether");
        require(!balance[msg.sender], "you staked hashis");
        require(!isEmpty, "There is no ether");

        time[msg.sender] = block.timestamp;
        balance[msg.sender] = true;

        //you need to approve the staking contract for 50 tokens
        IERC20(hashiContract).transferFrom(msg.sender, address(this), 50 ether);
    }

    function withdraw() external {
        require(balance[msg.sender], "you don't stake hashis");
        balance[msg.sender] = false;
        IERC20(hashiContract).transfer(msg.sender, 50 ether);
    }

    function claimRewards() external {
        require(founded, "there is no ether");
        require(time[msg.sender] != 0, "you don't stake hashis");
        uint256 stakedDays = (block.timestamp - time[msg.sender]) / 86400; //86400 = 1 day
        uint256 amountToClaim = 0.5 ether * stakedDays;
        time[msg.sender] = 0;

        if(amountToClaim >= address(this).balance){
            amountToClaim = address(this).balance;
            isEmpty = true;
        }

        (bool success, ) = address(msg.sender).call{value: amountToClaim}("");
        require(success, "incorrect transfer");
    }
}
