// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.7.0 <0.9.0;

/**
 * @title Owner
 * @dev Set & change owner
 */
contract ImprovedOwner {

    address private owner;
    address private ownerCandidate;

    modifier isOwner() {
        require(msg.sender == owner, "Caller is not owner");
        _;
    }

    modifier isOwnerCandidate() {
        require(msg.sender == ownerCandidate, "Caller is not owner candidate");
        _;
    }

    constructor() {
        owner = msg.sender; 
    }

    function transferOwnership(address candidate) external isOwner {
        ownerCandidate = candidate;
    }

    function acceptOwnership() external isOwnerCandidate {
        owner = ownerCandidate;
    }

    function getOwner() external view returns (address) {
        return owner;
    }

    function getOwnerCandidate() external view returns (address) {
        return ownerCandidate;
    }
} 