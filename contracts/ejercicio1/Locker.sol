// SPDX-License-Identifier: GPL-3.0

pragma solidity 0.8.13;

contract Locker {
    address public deployer;
    mapping (address => uint) public balance;
    mapping (address => uint) public timeLocked;
    uint256 public fee;

    constructor() {
        deployer = msg.sender;
    }

    function deposit(uint256 time) external payable {
        require(time > block.timestamp, "time couldnt be in the past");
        require(time >= timeLocked[msg.sender], "time must be greater than timeLocked");
        balance[msg.sender] += msg.value;
        timeLocked[msg.sender] = time;
    }

    function withdraw() external payable {
        require(balance[msg.sender] > 0, "There is no ether to withdraw");
        require(block.timestamp >= timeLocked[msg.sender], "time must be greater than timeLocked");

        balance[msg.sender] = 0;
        timeLocked[msg.sender] = 0;

        uint256 deployerAmount = (balance[msg.sender] * fee) / 100;
        uint256 msgSenderAmount = balance[msg.sender] - deployerAmount;

        (bool success1, ) = deployer.call{value: deployerAmount}("");
        (bool success2, ) = msg.sender.call{value: msgSenderAmount}("");

        require(success1 && success2, "there was a problem with the transfers");
    }

    //Fee should be between 0 and 30, that represent 0% and 30%.
    function setFee(uint256 _fee) external {
        require(deployer == msg.sender, "just owner");
        require(_fee < 30, "incorrect new fee value, must be lower than 30");
        fee = _fee;
    }
}